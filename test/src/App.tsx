import React, { useState } from "react";

interface Param {
  id: number;
  name: string;
  type: "string";
}

interface ParamValue {
  paramId: number;
  value: string;
}

interface Model {
  paramValues: ParamValue[];
}

interface Props {
  params: Param[];
  model: Model;
}

const App: React.FC<Props> = ({ params, model }) => {
  const [paramValues, setParamValues] = useState<ParamValue[]>(
    model.paramValues
  );

  const handleInputChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    paramId: number
  ) => {
    const { value } = event.target;
    setParamValues((prevParamValues) =>
      prevParamValues.map((paramValue) =>
        paramValue.paramId === paramId ? { ...paramValue, value } : paramValue
      )
    );
  };

  const getModel = (): Model => {
    return {
      paramValues,
    };
  };

  return (
    <div>
      {params.map((param) => (
        <div key={param.id}>
          <label htmlFor={`param-${param.id}`}>{param.name}:</label>
          <input
            type="text"
            id={`param-${param.id}`}
            name={`param-${param.id}`}
            value={
              paramValues.find((paramValue) => paramValue.paramId === param.id)
                ?.value || ""
            }
            onChange={(event) => handleInputChange(event, param.id)}
          />
        </div>
      ))}
      <button onClick={() => console.log(getModel())}>Get Model</button>
    </div>
  );
};

export default App;
