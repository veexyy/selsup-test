import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App.tsx";
interface Param {
  id: number;
  name: string;
  type: "string";
}

interface ParamValue {
  paramId: number;
  value: string;
}

interface Model {
  paramValues: ParamValue[];
  colors: string[];
}
interface Props {
  params: Param[];
  model: Model;
}
const props: Props = {
  params: [
    {
      id: 1,
      name: "Назначение",
      type: "string",
    },
    {
      id: 2,
      name: "Длина",
      type: "string",
    },
  ],
  model: {
    paramValues: [
      {
        paramId: 1,
        value: "повседневное",
      },
      {
        paramId: 2,
        value: "макси",
      },
    ],
    colors: ["red", "blue"],
  },
};
ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <App params={props.params} model={props.model} />
  </React.StrictMode>
);
